#include <stdio.h>
int vsota (int n) {
    int a = 0;
    int orig = n;
    while (orig > 0) {
        a += orig % 10;
        orig /= 10;
    }
    return a;
}
int main() {
    int p, q, k;
    scanf("%d %d %d", &p, &q, &k);
    int a = p * q;
    for (int i = 0; i < k; i++) {
        a = vsota(a);
    }
    printf("%d\n", a);
    return 0;
}
