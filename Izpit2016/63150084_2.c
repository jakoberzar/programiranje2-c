#include <stdio.h>
#include <stdlib.h>
typedef struct _kvadrat {
    int x;
    int y;
    int l;
} kvadrat;
int polje[300][300];
int main() {
    int n;
    scanf("%d", &n);
    kvadrat **ks = malloc(sizeof(kvadrat*) * n);
    for (int i = 0; i < n; i++) {
        kvadrat *k = malloc(sizeof(kvadrat));
        scanf("%d %d %d", &(k->x), &(k->y), &(k->l));
        ks[i] = k;
        for (int j = 0; j < k->l; j++) {
            for (int m = 0; m < k->l; m++) {
                polje[k->x + 100 + j][k->y + 100 + m] += 1;
            }
        }
    }
    
    int *repeated = calloc((n + 1), sizeof(int));
    for (int x = 0; x < 300; x++) {
        for (int y = 0; y < 300; y++) {
            if (polje[x][y] == 0) continue;
            repeated[polje[x][y]] += 1;
        }
    }
    for (int i = 1; i <= n; i++) {
        printf("%d\n", repeated[i]);
    }
    return 0;
}
