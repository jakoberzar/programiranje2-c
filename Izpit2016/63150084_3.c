#include <stdio.h>
#include <stdlib.h>
int *sts;
int n;
int *used;
int zaporedje (int ind, int val) {
    int longest = 0;
    for (int i = ind; i < n; i++) {
        if (val % sts[i] == 0) {
            int num = (used[i] != -1 ? used[i] : zaporedje(i + 1, sts[i])) + 1;
            if (num > longest) {
                longest = num;
            }
        }
    }
    used[ind - 1] = longest;
    return longest;
}
int cmp(const void* a, const void *b) {
    return (*(int*)b - *(int*)a);
}

int main() {
    scanf("%d", &n);
    sts = malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &(sts[i]));
    }
    used = malloc(n * sizeof(int));
    for (int i = 0; i < n; i++) {
        used[i] = -1;
    }
    qsort(sts, n, sizeof(int), cmp);
    
    int longest = 0;
    for (int i = 0; i < n; i++) {
        int num = (used[i] != -1 ? used[i] : zaporedje(i + 1, sts[i])) + 1;
        if (num > longest) longest = num;
    }
    
    printf("%d\n", longest);
    return 0;
}
