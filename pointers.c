#include <stdio.h>
int *insert_sort(int *a, int len) {
    for (int i = 1; i < len; i++) {
        int j = i;
        int el = a[i];
        while((j > 0) && (a[j-1] < el)) {
            a[j] = a[j-1];
            j--;
        }
        a[j] = el;
    }
    return a;
}
int main() {
    int arr[] = {5, 4, 3, 6, 2, 8, 7};
    int len = 7;
    int *p = insert_sort(arr, len);
    for (int i = 0; i < len; i++) {
        printf("%d ", p[i]);
    }
    return 0;
}