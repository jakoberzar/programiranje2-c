#include <stdio.h>

int fibb(int n) {
    if (n == 1) return 1;
    if (n == 2) return 1;
    return fibb(n-1) + fibb(n-2);
}
int main() {
    int n;
    for(n = 1; n <= 10; n++)
        printf("%d ", fibb(n));
    printf("\n");
    return 0;
}

/*
$ gcc -o fibb fibb.c
$ ./fibb
1 1 2 3 5 8 .. 89
$

Če zamenjamo n == 1 in n == 2 na n = 1 in n = 2
Najde dobimo 2 warninga in 4 note ko compilamo in ko poženemo dobimo 1 1 1 1 1 1 1 1 1

C da čudne errorje...
*/