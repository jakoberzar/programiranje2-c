public class Fibb {
    public static void main (String args[]) {
        for (int n = 1; n <= 10; n++)
            System.out.printf("%d ", fibb(n));
        System.out.printf("\n");
    }
    
    public static int fibb(int n) {
        if (n == 1) return 1;
        if (n == 2) return 1;
        return fibb(n-1) + fibb(n-2);
    }
}

/* 
$ javac Fibb.java
$ java Fibb
1 1 2 3 5 8 ... 89
$

Če bi spremenu n == 1 in n == 2 v n = 1 in n = 2,
bi dala 2 errorja, da je v pogoju integer in ne boolean

Java ti za errorje lepo pove
*/