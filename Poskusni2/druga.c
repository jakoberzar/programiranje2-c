#include <stdio.h>
#include <stdlib.h>

typedef struct _node { int value; struct _node *next; } node;
node *filterIterative (int max, node *list) {
    node* prva = NULL;
    node* prejsnja = NULL;
    node* trenutna = list;
    while (trenutna) {
        while (trenutna && trenutna->value > max) {
            trenutna = trenutna->next;
        } // dob tako daj uredu
        
        if (prejsnja == NULL) {
            prva = trenutna;
        } else {
            prejsnja->next = trenutna;
        }
        prejsnja = trenutna;
        trenutna = trenutna ? trenutna->next : NULL;
    }
    return prva;
}

// Rekurzivna
node *filter (int max, node *list) {
    if (list == NULL) return NULL;
    else {
        list->next = filter(max, list->next);
        if (list->value <= max) return list;
        else {
            node* n = list->next;
            free(list);
            return n;
        }
    }
}

void dump(node *list) {
    if (list) {
        printf("%d\n", list->value);
        dump(list->next);
    }
}

int main() {
    node *a = malloc(sizeof(node));
    a->value = 4;
    node *b = malloc(sizeof(node));
    b->value = 6;
    node *c = malloc(sizeof(node));
    c->value = 7;
    node *d = malloc(sizeof(node));
    d->value = 5;
    a->next = b;
    b->next = c;
    c->next = d;
    d->next = NULL;
    dump(a);
    node *n = filterIterative(5, a);
    printf("Filtered\n");
    dump(n);
}