#include <stdio.h>
#include <stdlib.h>
#define NULA NULL
typedef struct _node {
    int value;
    struct _node *next;
} node;
void dump(node *list) {
    if (list) {
#ifdef NULK
        printf("%d\n", list->value);
#else
        printf("%d, ", list->value);
#endif
        dump(list->next);
    }
}


node* insert_a(node* list, int value) {
    node *nova = (node*)malloc(sizeof(node));
    nova->value = value;
    nova->next = list;
    return nova;
}

node *insert_z(node *list, int value) {
    if (!list) {
        node *nov = (node*)malloc(sizeof(node));
        nov->value = value;
        nov->next = NULA;
        return nov;
    } else {
        list->next = insert_z(list->next, value);
        return list;
    }
}

node *insert_z_iter(node *list, int value) {
    node *trenutna = list;
    node *prejsnja = NULA;
    node *prva = list;
    while (trenutna) {
        prejsnja = trenutna;
        trenutna = trenutna->next;
    }
    node *nov = (node*)malloc(sizeof(node));
    nov->value = value;
    nov->next = NULA;
    if (prejsnja) prejsnja->next = nov;
    if (!prva) prva = nov;
    return prva;
}

node *insert_at(node *list, int value, int ttl) {
    if (!list || !ttl) {
        node *nov = (node*)malloc(sizeof(node));
        nov->value = value;
        nov->next = !list ? NULA : list;
        return nov;
    } else {
        list->next = insert_at(list->next, value, ttl - 1);
        return list;
    }
}

node *insert_ord(node *list, int value) {
    if (!list || value < list->value) {
        node *nov = (node*)malloc(sizeof(node));
        nov->value = value;
        nov->next = !list ? NULA : list;
        return nov;
    } else {
        list->next = insert_ord(list->next, value);
        return list;
    }
}

node *insert_ord_list(node *list, node* valNode) {
    if (!list || valNode->value < list->value) {
        valNode->next = !list ? NULA : list;
        return valNode;
    } else {
        list->next = insert_ord_list(list->next, valNode);
        return list;
    }
}

node *sort(node *list) {
    if (!list) return NULA;
    else {
        node* next = sort(list->next);
        node* added = insert_ord_list(next, list);
        return added;
    }
}

int main() {
    node *a = NULA;
    a = insert_z(a, 1);
    node *b = insert_a(a, 3);
    node *c = insert_z_iter(b, 99);
    node *d = insert_at(c, 2, 1);
    node *e = insert_ord(d, 4);
    node *f = sort(e);
    dump(f);
}