#include <stdio.h>
#include <stdlib.h>
typedef struct _node { int value; struct _node *left, *right; } node;
void dump_heap (FILE *file, node *heap) {
    dump_under(file, heap, 8);
}

int dump_under (FILE *file, node *heap, int limit) {
    if (heap == NULL) return 0;
    else {
        int levo = dump_under(file, heap->left, limit);
        int desno = dump_under(file, heap->right, limit);
        int sum = levo + desno;
        if (sum > limit) {
            fprintf(file, "%d\n", heap->value);
        }
        return sum + 1;
    }
}

int main() {
    FILE *ff = fopen("lel.txt", "w");
    
    node *a = malloc(sizeof(node));
    a->value = 1;
    node *b = malloc(sizeof(node));
    b->value = 2;
    node *c = malloc(sizeof(node));
    c->value = 3;
    node *d = malloc(sizeof(node));
    d->value = 4;
    node *e= malloc(sizeof(node));
    e->value = 5;
    node *f = malloc(sizeof(node));
    f->value = 6;
    node *g = malloc(sizeof(node));
    g->value = 7;
    node *h = malloc(sizeof(node));
    h->value = 8;
    node *i = malloc(sizeof(node));
    i->value = 9;
    
    /*h->left = NULL;
    h->right = NULL;
    i->left = NULL;
    i->right = NULL;*/
    g->left = h;
    g->right = i;
    e->left = f;
    e->right = g;
    a->left = NULL;
    a->right = b;
    b->left = c;
    b->right = e;
    c->left = d;
    
    dump_heap(stdout, a);
    fprintf(stdout, "test\n");
    fclose(ff);
}