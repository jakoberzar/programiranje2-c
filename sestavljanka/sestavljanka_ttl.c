/* --------
|  Najde resitev brez uporabe arraya, temvec ima mejo kako globoko gre lahko (ttl).
\* -------- */
// Vrstica za spremembo zacetne pozicije: 193
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void dump();
unsigned int toUInt();
int solved();
int unseen();
int solve(int x, int y, int ttl);
int count_used_places();

int t[3][3];    // Current table, stored as a global for easier access
int sprint = 0; // Should the program print additional debug data?
int sanalyze = 1; // Let the program do some analitics after solving.

// Dumps the current state of the puzzle
void dump() {
    printf("%d %d %d\n", t[0][0], t[0][1], t[0][2]);
    printf("%d %d %d\n", t[1][0], t[1][1], t[1][2]);
    printf("%d %d %d\n", t[2][0], t[2][1], t[2][2]);
    printf("\n");
}

// Converts the current state to an integer
unsigned int toUInt() {
    unsigned int a = 0;
    // Namesto vpisavanja dejanske vrednosti in deljenja z vsakim loopom sem
    // poskusil pow(10, 8 - i), kar pa je upocasnilo program za kar 2x!
    // Brez loopa (9x napisano a+= t[...] * 10...) pa se niti ne pozna.
    int ten = 100000000;
    for (int i = 0; i < 9; i++) {
        a += t[i / 3][i % 3] * ten;
        ten /= 10;
    }
    return a;
}

// Check if the current state equals the solved state. Return 1 if it does, else 0
int solved() {
    for (int i = 0; i < 8; i++) {
        if (t[i / 3][i % 3] != i + 1) return 0;
    }
    printf("Result:\n");
    dump();
    printf("...it's done!\n");
    return 1;
}

// Returns 1 if the current state hasn't been seen yet, else returns 0.
int unseen(int ttl) {
    return ttl < 50;
}

int solve(int x, int y, int ttl) {
    ttl++;
    if (sprint) {
        printf("Start with:");
        dump();
    }
    if (solved()) {
        return 0;
    }
    if (x > 0) {
        t[x][y] = t[x-1][y];
        t[x-1][y] = 0;
        if (unseen(ttl)) {
            int rund = solve(x-1, y, ttl);
            if (rund >= 0) return rund;
        } else {
            if (sprint) printf("Tried move up but didn't work!\n");
        }
        t[x-1][y] = t[x][y];
        t[x][y] = 0;
        if (sprint) printf("End moving up\n");
    }
    if (y > 0) {
        t[x][y] = t[x][y-1];
        t[x][y-1] = 0;
        if (unseen(ttl)) {
            int rund = solve(x, y-1, ttl);
            if (rund >= 0) return rund;
        } else {
            if (sprint) printf("Tried move left but didn't work!\n");
        }
        t[x][y-1] = t[x][y];
        t[x][y] = 0;
        if (sprint) printf("End moving left\n");
    }
    if (x < 2) {
        t[x][y] = t[x+1][y];
        t[x+1][y] = 0;
        if (unseen(ttl)) {
            int rund = solve(x+1, y, ttl);
            if (rund >= 0) return rund;
        } else {
            if (sprint) printf("Tried move down but didn't work!\n");
        }
        t[x+1][y] = t[x][y];
        t[x][y] = 0;
        if (sprint) printf("End moving down\n");
    }
    if (y < 2) {
        t[x][y] = t[x][y+1];
        t[x][y+1] = 0;
        if (unseen(ttl)) {
            int rund = solve(x, y+1, ttl);
            if (rund >= 0) return rund;
        } else {
            if (sprint) printf("Tried move right but didn't work!\n");
        }
        t[x][y+1] = t[x][y];
        t[x][y] = 0;
        if (sprint) printf("End moving right\n");
    }
    return -1;
}

int count_used_places() {
    if (!sanalyze) return -1;
    printf("\nCounting used array places, statistics purposes :)\n");
    return 0;
}

void init_easiest() {
    t[0][0] = 1;
    t[0][1] = 2;
    t[0][2] = 3;
    t[1][0] = 4;
    t[1][1] = 0;
    t[1][2] = 6;
    t[2][0] = 7;
    t[2][1] = 5;
    t[2][2] = 8;
}

void init_easy() {
    t[0][0] = 7;
    t[0][1] = 1;
    t[0][2] = 4;
    t[1][0] = 5;
    t[1][1] = 8;
    t[1][2] = 2;
    t[2][0] = 6;
    t[2][1] = 3;
    t[2][2] = 0;
}

void init_fast() {
    t[0][0] = 8;
    t[0][1] = 7;
    t[0][2] = 6;
    t[1][0] = 5;
    t[1][1] = 4;
    t[1][2] = 3;
    t[2][0] = 2;
    t[2][1] = 1;
    t[2][2] = 0;
}

void init_some() {
    t[0][0] = 2;
    t[0][1] = 0;
    t[0][2] = 8;
    t[1][0] = 6;
    t[1][1] = 1;
    t[1][2] = 7;
    t[2][0] = 4;
    t[2][1] = 5;
    t[2][2] = 3;
}

int find0_x() {
    for (int i = 0; i < 9; i++) if (t[i / 3][i % 3] == 0) return i / 3;
    printf("Couldn't find 0!!!\n");
    return -1;
}

int find0_y() {
    for (int i = 0; i < 9; i++) if (t[i / 3][i % 3] == 0) return i % 3;
    printf("Couldn't find 0!!!\n");
    return -1;
}

int main() {
    // Create the massive array
    
    // Initialize the starting puzzle
    init_fast(); // (Choose a method above)
    printf("Origin:\n");
    dump();
    
    // Solve the puzzle
    clock_t start = clock();
    int result = solve(find0_x(), find0_y(), 0);
    clock_t stop = clock();
    if (result == -1) printf("\nSorry, couldn't solve it!");
    
    // Do some analitics
    int used = count_used_places();
    double usedprecent = (double)used / (double)1000000000 * 100;
    clock_t analitics = clock();
    
    // Calculate the time in miliseconds
    int msstart = (int)(start / (CLOCKS_PER_SEC  / 1000));
    int msstop = (int)(stop / (CLOCKS_PER_SEC  / 1000));
    int msexec = (int)((stop - start) / (CLOCKS_PER_SEC  / 1000));
    int msanal = (int)(analitics / (CLOCKS_PER_SEC  / 1000));
    int msanalstop = (int)((analitics - stop) / (CLOCKS_PER_SEC  / 1000));
    
    // Print the results
    printf("\n/* ------- ARRAY INFO ------- *\\\n");
    printf("| Used bytes: %13d B  |\n", used);
    printf("| Percent used:   %3.7lf %%  |\n", usedprecent);
    printf("| Analitic time: %10d ms |", msanalstop);
    printf("\n| ------- SOLVING INFO ------- |\n");
    printf("| Clock Start: %12d ms |\n", msstart);
    printf("| Clock End:   %12d ms |\n", msstop);
    printf("| Execution:  %13d ms |\n", msexec);
    printf("\\* --------- KTHXBAI -------- */\n");
    
    return 0;
}