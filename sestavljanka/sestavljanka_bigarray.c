/* --------
|  Najde resitev z uporabo zelo velikega arraya (44MB) - Vcasih je bil velik,
|  ker je porabil 1 GB :)
\* -------- */
// Vrstica za spremembo zacetne pozicije :240
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void dump();
unsigned int toUInt();
int solved();
int unseen();
int solve(int x, int y);
int count_used_places();

char *combs;    // Pointer to the array of all combinations (niso glavniki!)
int t[3][3];    // Current table, stored as a global for easier access
int sprint = 0; // Should the program print additional debug data?
int sanalyze = 1; // Let the program do some analitics after solving.

// Dumps the current state of the puzzle
void dump() {
    printf("%d %d %d\n", t[0][0], t[0][1], t[0][2]);
    printf("%d %d %d\n", t[1][0], t[1][1], t[1][2]);
    printf("%d %d %d\n", t[2][0], t[2][1], t[2][2]);
    printf("\n");
}

// Converts the current state to an integer
/* ------ Optimizacija iz 1GB na 44MB:
|  Prvotno je array porabil 1GB RAMa, po logiki - 9 stevil je, torej potrebujem
|  10^9 RAMa, ce hocem zapisati stevilo 123456780. Po premisleku danes, kako bi
|  lahko ostevilcil permutacije po vrsti, pa sem prisel do vsaj delne optimizacije.
|  - Zadnje mesto sploh ni potrebno shranjevati
|  - Lahko namesto baze 10 uporabim bazo 9, ker so stevila od 0 do 8.
|  Tako nisem zgubil na casu pri pretvorbi (se malce pridobil!) in porabil
|  razumljivo kolicino RAMa.
\* ------ */
unsigned int toUInt() {
    unsigned int a = 0;
    int current_base = 4782969; // 9^7
    for (int i = 0; i < 8; i++) {
        a += t[i / 3][i % 3] * current_base;
        current_base /= 9;
    }
    return a;
}

// Check if the current state equals the solved state. Return 1 if it does, else 0
int solved() {
    for (int i = 0; i < 8; i++) {
        if (t[i / 3][i % 3] != i + 1) return 0;
    }
    printf("Result:\n");
    dump();
    printf("...it's done!\n");
    return 1;
}

// Returns 1 if the current state hasn't been seen yet, else returns 0.
int unseen() {
    unsigned int a = toUInt();
    int t = combs[a];
    combs[a] = 1;
    return 1 - t;
}

// Moves the number on newX,newY to the current x,y(zero) and tries to solve it
/* ------
 | To funkcijo sem zelel uporabiti skupaj s funkcijo solve v rekurziji,
 | vendar sem v 1GB virtualki zacel dobivati Segmentation fault, ko sem jo 
 | implementiral na vseh 4 smereh. Na 2/4 se vcasih deluje, 4/4 pa nikoli.
 | Sklepam, da zaradi se dodatnih klicev zmanjka prostora na stacku (2x hitreje)?
 | Zato te funkcije ubistvu nikoli ne klicem.
\* ---- */
int move(int x, int y, int newX, int newY) {
    int ret = -1;
    t[x][y] = t[newX][newY];
    t[newX][newY] = 0;
    if (unseen()) {
        ret = solve(newX, newY);
    }
    t[newX][newY] = t[x][y];
    t[x][y] = 0;
    return ret;
}

int solve(int x, int y) {
    if (sprint) {
        printf("Start with:");
        dump();
    }
    if (solved()) {
        return 0;
    }
    if (x > 0) {
        /* Z uporabo zgornje funkcije bi bilo le to:
        int m = move(x, y, x - 1, y);
        if (m >= 0) return m;
        */
        t[x][y] = t[x-1][y];
        t[x-1][y] = 0;
        if (unseen()) {
            int rund = solve(x-1, y);
            if (rund >= 0) return rund;
        } else {
            if (sprint) printf("Tried move up but didn't work!\n");
        }
        t[x-1][y] = t[x][y];
        t[x][y] = 0;
        if (sprint) printf("End moving up\n");
    }
    if (y > 0) {
        t[x][y] = t[x][y-1];
        t[x][y-1] = 0;
        if (unseen()) {
            int rund = solve(x, y-1);
            if (rund >= 0) return rund;
        } else {
            if (sprint) printf("Tried move left but didn't work!\n");
        }
        t[x][y-1] = t[x][y];
        t[x][y] = 0;
        if (sprint) printf("End moving left\n");
    }
    if (x < 2) {
        t[x][y] = t[x+1][y];
        t[x+1][y] = 0;
        if (unseen()) {
            int rund = solve(x+1, y);
            if (rund >= 0) return rund;
        } else {
            if (sprint) printf("Tried move down but didn't work!\n");
        }
        t[x+1][y] = t[x][y];
        t[x][y] = 0;
        if (sprint) printf("End moving down\n");
    }
    if (y < 2) {
        t[x][y] = t[x][y+1];
        t[x][y+1] = 0;
        if (unseen()) {
            int rund = solve(x, y+1);
            if (rund >= 0) return rund;
        } else {
            if (sprint) printf("Tried move right but didn't work!\n");
        }
        t[x][y+1] = t[x][y];
        t[x][y] = 0;
        if (sprint) printf("End moving right\n");
    }
    return -1;
}

// Count the amount of combinations actually found
int count_used_places() {
    if (!sanalyze) return -1;
    printf("\nCounting used array places, statistics purposes :)\n");
    int counter = 0;
    for (int i = 0; i < 43046721; i++) if (combs[i]) counter++;
    return counter;
}

// Requires 2 moves to solve in real life
void init_easy() {
    t[0][0] = 1;
    t[0][1] = 2;
    t[0][2] = 3;
    t[1][0] = 4;
    t[1][1] = 0;
    t[1][2] = 6;
    t[2][0] = 7;
    t[2][1] = 5;
    t[2][2] = 8;
}

// A random sample from the internet
void init_one() {
    t[0][0] = 7;
    t[0][1] = 1;
    t[0][2] = 4;
    t[1][0] = 5;
    t[1][1] = 8;
    t[1][2] = 2;
    t[2][0] = 6;
    t[2][1] = 3;
    t[2][2] = 0;
}

// Gets solved pretty fast!
void init_fast() {
    t[0][0] = 8;
    t[0][1] = 7;
    t[0][2] = 6;
    t[1][0] = 5;
    t[1][1] = 4;
    t[1][2] = 3;
    t[2][0] = 2;
    t[2][1] = 1;
    t[2][2] = 0;
}

// Feel free to change it!
void init_some() {
    t[0][0] = 2;
    t[0][1] = 0;
    t[0][2] = 8;
    t[1][0] = 6;
    t[1][1] = 1;
    t[1][2] = 7;
    t[2][0] = 4;
    t[2][1] = 5;
    t[2][2] = 3;
}

// Find the zero in the table (X coordinate)
int find0_x() {
    for (int i = 0; i < 9; i++) if (t[i / 3][i % 3] == 0) return i / 3;
    printf("Couldn't find 0!!!\n");
    return -1;
}

// Find the zero in the table (Y coordinate)
int find0_y() {
    for (int i = 0; i < 9; i++) if (t[i / 3][i % 3] == 0) return i % 3;
    printf("Couldn't find 0!!!\n");
    return -1;
}

int main() {
    // Create the (not so massive anymore) array
    combs = (char *)calloc(43046721, sizeof(char)); // Improved from 1GB to 44MB!
    if (combs == NULL) {
        printf("Not enough memory to make the giant array!\n");
        return 1;
    }
    
    // Initialize the starting puzzle
    init_easy(); // (Choose a method above)
    
    printf("Origin:\n");
    dump();
    
    // Solve the puzzle
    clock_t start = clock();
    int result = solve(find0_x(), find0_y());
    clock_t stop = clock();
    if (result == -1) printf("\nSorry, couldn't solve it!");
    
    // Do some analitics
    int used = count_used_places();
    double usedprecent = (double)used / (double)43046721 * 100;
    free(combs);
    clock_t analitics = clock();
    
    // Calculate the time in miliseconds
    int msstart = (int)(start / (CLOCKS_PER_SEC  / 1000));
    int msstop = (int)(stop / (CLOCKS_PER_SEC  / 1000));
    int msexec = (int)((stop - start) / (CLOCKS_PER_SEC  / 1000));
    int msanal = (int)(analitics / (CLOCKS_PER_SEC  / 1000));
    int msanalstop = (int)((analitics - stop) / (CLOCKS_PER_SEC  / 1000));

    // Print the results
    printf("\n/* ------- ARRAY INFO ------- *\\\n");
    printf("| Used bytes: %13d B  |\n", used);
    printf("| Percent used:   %3.7lf %%  |\n", usedprecent);
    printf("| Analitic time: %10d ms |", msanalstop);
    printf("\n| ------- SOLVING INFO ------- |\n");
    printf("| Initialization: %9d ms |\n", msstart);
    printf("| Solving end: %12d ms |\n", msstop);
    printf("| Execution:  %13d ms |\n", msexec);
    printf("\\* --------- KTHXBAI -------- */\n");
    
    return 0;
}