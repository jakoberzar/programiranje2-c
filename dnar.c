#include <stdio.h>
int rekt(int *k, int ki, int n);
int main() {
    int n;
    float fl;
    scanf("%f", &fl);
    n = (int)(fl * 100);
    int k[] = {1, 2, 5, 10, 20, 50, 100, 200}; // kovanci
    int ways = rekt(k, 7, n);
    printf("%d\n", ways);
}

int rekt(int *k, int ki, int n) {
    if (n == 0) return 1;
    if (ki < 0) return 0;
    if (ki == 0) return 1;
    int rem = n;
    int ways = 0;
    while (rem >= k[ki]) {
        rem -= k[ki];
        ways += rekt(k, ki - 1, rem);
    }
    ways += rekt(k, ki - 1, n);
    //printf("k: %d n: %d ways: %d\n", k[ki], n, ways);
    return ways;
}