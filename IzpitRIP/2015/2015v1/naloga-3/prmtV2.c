#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int n, d, sum=0;
void permute(char *c, int index){
	if(index==n){
		int j=1;
	    for(int i=1; i<=n; i++)
			if(abs(c[i]-c[i-1])>d){ j=0; break; }
	    if(j){
			sum++;
//			printf("%s\n", c);
		}
	}else
        for(int i=index; i<=n; i++){
			char c2[n+1], tmp;
			strcpy(c2, c);
			
			tmp=c[index];
			c[index]=c[i];
			c[i]=tmp;

			permute(c, index+1);
			strcpy(c, c2);
		}
}

int main(){
	scanf("%d %d", &n, &d);

    char c[n+1];
	for(int i=0; i<n; i++)
		c[i]=i+49;
	c[n]='\0';

	n--;
	permute(c, 0);
	printf("%d\n", sum);
    return 0;
}
// gcc -std=c99 -pedantic -Wall -o prmt prmt.c -lm
// ./prmt < input-1