#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int sum=0;
void swap(char *x, char *y){
    char temp = *x;
    *x = *y;
    *y = temp;
}
void permute(char *a, int l, int r, int d){
	if(l==r){
		int j=1;
	    for(int i=1; i<=r; i++)
			if(abs(a[i]-a[i-1])>d){ j=0; break; }
	    if(j){
			sum++;
//			printf("%s\n", a);
		}
	}else
        for(int i = l; i <= r; i++){
           swap((a+l), (a+i));
           permute(a, l+1, r, d);
           swap((a+l), (a+i));
		}
}

int main(){
	int i, n, d;
	scanf("%d %d", &n, &d);

    char str[n+1];
	for(i=0; i<n; i++)
		str[i]=i+49;
	str[i]='\0';

	permute(str, 0, strlen(str)-1, d);
	printf("%d\n", sum);
    return 0;
}
// gcc -std=c99 -pedantic -Wall -o prmt prmt.c -lm
// ./prmt < input-1