#include <stdio.h>
#include <stdlib.h>

int main() {
    char name[110];
    int n;
    FILE *f = fopen("inp-ex3", "r");
    fscanf(f, "%s %d", name, &n);
    //printf("my n is %d", n);
    FILE *f2 = fopen(name, "r");
    char repet[256] = {};
    char original[256];
    for (int i = 0; i < 256; i++) original[i] = i;
    while (feof(f2) == 0) {
        char c;
        fscanf(f2, "%c", &c);
        repet[c] += 1;
    }
    
    for (int i = 0; i < 256; i++) {
        int max = i;
        for (int j = i+1; j < 256; j++) {
            if (repet[j] > repet[max]) max = j;
        }
        char temp = repet[i];
        repet[i] = repet[max];
        repet[max] = temp;
        temp = original[i];
        original[i] = original[max];
        original[max] = temp;
    }
    //printf("%d\n", n);
    printf("%d\n", repet[n-1]);
    /*for (int i = 0; i < 256; i++) {
        printf("%d. %dx: %d\n", i, repet[i], original[i]);
    }*/
    fclose(f);
    fclose(f2);
    return 0;
}