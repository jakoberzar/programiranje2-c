#include <stdio.h>
#include <stdlib.h>
#define g 10000

int main(){
	int x, glm=1000, i, j, s=0, tmp, *t=(int*)malloc(glm*sizeof(int)), *e=(int*)malloc(glm*sizeof(int));
	unsigned char c2; char c[110];
	scanf("%s\n%d", c, &x);
	FILE *f1=fopen(c, "rb");
	
	while((fscanf(f1, "%c", &c2))!=EOF){
		tmp=0;
		for(i=0; i<s; i++)
			if(c2==t[i]){
				tmp=1; e[i]++;
			}
		if(tmp) continue;
		
		t[s]=c2; e[s]=1; s++;
		if(s>=glm){
			glm+=g;
			t=(int*)realloc(t, glm*sizeof(int));
			e=(int*)realloc(e, glm*sizeof(int));
		}
	}
	
	for(i=0; i<s; i++)
		for(j=i+1; j<s; j++)
			if(e[i]<e[j]){
				tmp=e[i];
				e[i]=e[j];
				e[j]=tmp;
				tmp=t[i];
				t[i]=t[j];
				t[j]=tmp;
			}
	printf("%d\n", e[x-1]);

	return 0;
}

// gcc -std=c99 -pedantic -Wall -o c123 c.c -lm
// ./c123 < inp-ex1