#include <stdio.h>
#include <stdlib.h>
int x;
int rnd(int n) {
    return (75 * n) % 65537;
}

int placeVal(int i, int j) {
    if (i == 0 && j == 0)
        return rnd(1);
    else if (i == 0 && j > 0) 
        return rnd(placeVal(x-1, j-1));
    else 
        return rnd(placeVal(i-1, j));
    //else return rnd(0);
}


int main() {
    FILE *f = fopen("inp-ex3", "r");
    int y, m, s;
    fscanf(f, "%d %d %d %d", &x, &y, &m, &s);
    int **p = malloc(sizeof(int*) * x);
    for (int i = 0; i < x; i++) {
        int *d = malloc(sizeof(int) * y);
        for (int j = 0; j < y; j++) {
            d[j] = placeVal(i, j) % m;
        }
        p[i] = d;
    }
    fclose(f);
    for (int i = 0; i < y; i++) {
        for (int j = 0; j < x; j++) {
            printf("%d ", p[j][i]);
        }
        printf("\n");
    }
    int count = 0;
    for (int i = 0; i <= x - s; i++) {
        for (int j = 0; j <= y - s; j++) {
            int *found = calloc(s * s, sizeof(int));
            int found0 = 0;
            int foundAny = 0;
            printf("Kvadrat1:\n");
            for (int i1 = i; i1 < i + s; i1++) {
                printf("\n");
                for (int j1 = j; j1 < j + s; j1++) {
                    int myi =  (i1 - i) * s + (j1 - j);
                    int a = p[i1][j1];
                    printf("%d ", a);
                    if (a == 0) {
                        if (found0) foundAny = 1;
                        else found0 = 1;
                    }
                    for (int d = 0; d < myi; d++) {
                        if (a == found[d]) {
                            foundAny = 1;
                        }
                    }
                    found[myi] = a;
                    if (foundAny) break;
                }
            }
            printf("found:%d\n\n", foundAny);
            free(found);
            if (!foundAny) count++;
        }
    }
    printf("%d\n", count);
    return 0;
}