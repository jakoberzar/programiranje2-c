#include <stdio.h>
#include <stdlib.h>
char vals[20000000];
int main() {
    char name[20+1];
    scanf("%s", name);
    FILE *f = fopen(name, "r");
    int n;
    int sredina = 10000000;
    while (!feof(f)) {
        fscanf(f, "%d", &n);
        vals[sredina +n] = 1;
    }
    fclose(f);
    int max = 0;
    int diff = 0;
    int first = 1;
    for (int i = 0; i < 20000000; i++) {
        if (vals[i] == 1) {
            if (first) { 
                first = 0;
            } else {
                diff++;
                if (diff > max) max = diff;
            }
            diff = 0;
        } else {
            diff++;
        }
    }
    printf("%d", max);
}