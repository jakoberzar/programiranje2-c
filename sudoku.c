#include <stdio.h>

void dump(int *a);
int check(int *a, int i, int n);
int check_hor(int *a, int i, int n);
int check_ver(int *a, int i, int n);
int check_sq(int *a, int i, int n);
int tryNext(int *a, int i);

int fill123(int *a, int i) {
    int v = i / 9;
    int s = i % 9;
    int v0 = v - v % 3;
    int s0 = s - s % 3;
    int c = 1;
    for (int vi = v0; vi < (v0 + 3); vi++) {
        for (int si = s0; si < (s0 + 3); si++) {
            a[vi * 9 + si] = c;
            c++;
        }
    }
}

int main() {
    int a[81];
    for (int i = 0; i < 81; i++) {
        a[i] = 0;
    }
    fill123(a, 0);
    fill123(a, 40);
    fill123(a, 79);
    dump(a);
    int res = tryNext(a, 0);
    if (res) {
        dump(a);
    } else {
        printf("couldn't solve it :(\n");
    }
    return 0;
}

int tryNext(int *a, int i) {
    int emptyNext = 81;
    for (int place = i; place < 81; place++) {
        if (a[place] == 0) {
            emptyNext = place;
            break;
        }
    }
    if (emptyNext == 81) {
        return 1;
    }
    
    int tryNumber = 10;
    for (int number = 1; number < 10; number++) {
        int c = check(a, emptyNext, number);
        if (c) {
            a[emptyNext] = number;
            int res = tryNext(a, emptyNext + 1);
            if (res) {
                return 1;
            }
            a[emptyNext] = 0;
        }
    }
    return 0;
}


int check(int *a, int i, int n) {
    return check_hor(a, i, n) && check_ver(a, i, n) && check_sq(a, i, n);
}
int check_hor(int *a, int i, int n) {
    int v = i / 9;
    for (int place = v * 9; place < (v + 1) * 9; place++) {
        if (a[place] == n) return 0;
    }
    return 1;
}
int check_ver(int *a, int i, int n) {
    int v = i % 9;
    for (int place = v; place < 81; place += 9) {
        if (a[place] == n) return 0;
    }
    return 1;
}
int check_sq(int *a, int i, int n) {
    int v = i / 9; // 1
    int s = i % 9; // 1
    int v0 = v - v % 3;
    int s0 = s - s % 3;
    for (int vi = v0; vi < (v0 + 3); vi++) {
        for (int si = s0; si < (s0 + 3); si++) {
            if (a[vi * 9 + si] == n) return 0;
        }
    }
    return 1;
}

void dump(int *a) {
    printf(" -----------------------\n");
    for (int i = 0; i < 81; i++) {
        if (i % 9 == 0) printf("| ");
        printf("%d ", a[i]);
        if ((i + 1) % 3 == 0) printf("| ");
        if ((i + 1) % 9 == 0) printf("\n");
        if ((i + 1) % 27 == 0) printf(" -----------------------\n");
    }
}