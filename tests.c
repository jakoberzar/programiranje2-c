#include <stdio.h>
#include <math.h>
int isPrime(int n) {
    for (int i = 2; i <= sqrt(n); i++) {
        if (n % i == 0) return 0;
    }
    return 1;
}
int main() {
    int a[2][2][3] = {{{0,1,2},{4,5,6}},{{7,8,9},{10,11,12}}};
    int *p = &(a[0][0][0]);
    for (int i = 0; i < 12; i++) {
        printf("%d\n", p[i]);
    }
    printf("second\n");
    p = (a[0][0]);
    for (int i = 0; i < 12; i++) {
        printf("%d\n", *(p + i));
    }
    
    printf("\nPrimes\n");
    for (int i = 2; i < 200; i++) {
        if (isPrime(i)) printf("%d\n", i);
    }
    
    int a[5];
    for (int i = 0; i < 5; i++) {
        printf("%d\n", a[i];)
    }
}