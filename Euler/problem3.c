#include <stdio.h>
#include <math.h>
int isPrime(int n) {
    for (int i = 2; i <= sqrt(n); i++) {
        if (n % i == 0) return 0;
    }
    return 1;
}
int main() {
    long a = 600851475143;
    long max = 0;
    int till = floor(sqrt(a));
    int count = 2;
    while (a > count) {
        if (a % count == 0 && isPrime(count)) {
            a /= count;
        } else {
            count++;
        }
    }
    printf("%d", count);
}