#include <stdio.h>
int checkifpol(int a) {
    int b = a;
    int invert = 0;
    while (a > 0) {
        invert *= 10;
        invert += a % 10;
        a -= a % 10;
        a /= 10;
    }
    return invert == b;
}
int main() {
    int max = 0;
    for (int i = 100; i < 999; i++) {
        for (int j = i; j < 999; j++) {
            int product = i * j;
            if (checkifpol(product) && product > max) max = product;  
        }
    }
    printf("%d\n", max);
}