#include <stdio.h>
int main() {
    for (int a = 1; a < 1000; a++) {
        int rest = 1000 - a;
        for (int b = a + 1; b < 1000; b++) {
            rest -= b;
            long s1 = (long)a * a + (long)b * b;
            long s2 = rest * rest;
            if (s1 == s2) {
                printf("Found: %d %d %d\n", a, b, rest);
                printf("%ld", (long) a * b * rest);
                return 0;
            }
            rest += b;
        }
    }
}