#include <stdio.h>
#include <stdlib.h>

int maxR;
int f(int **t, int r, int c) {
    if (r > maxR) return 0;
    int levo = f(t, r + 1, c);
    int desno = f(t, r + 1, c + 1);
    int max = levo > desno ? levo : desno;
    return max + t[r][c];
}


int main() {
    int n;
    int r = 0, c = 0;
    int **t = NULL;
    while (scanf("%d", &n) != EOF) {
        if (c == 0) {
            t = realloc(t, sizeof(int *) * (r + 1));
            t[r] = malloc((r + 1) * sizeof(int));
        }
        t[r][c] = n;
        c++;
        if (c > r) {
            c = 0;
            r++;
        }
    }
    maxR = r;
    int res = f(t, 0, 0);
    printf("%d\n", res);
}