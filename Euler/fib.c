#include <stdio.h>
int main() {
    long a = 1;
    long b = 1;
    long sum = 0;
    while (b < 4000000) {
        long c = a + b;
        a = b;
        b = c;
        if (c < 4000000 && c % 2 == 0) sum += c;
    }
    printf("%ld", sum);
}