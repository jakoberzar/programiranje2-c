#include <stdio.h>
#include <math.h>

int isPrime(int n) {
    for (int i = 2; i <= sqrt(n); i++) {
        if (n % i == 0) return 0;
    }
    return 1;
}

int main() {
    long sum = 0;
    for (int i = 2; i < 2000000; i++) {
        if (isPrime(i)) sum += i;
    }
    printf("%ld", sum);
}