#include <stdio.h>
int main() {
    int a = 20;
    while (1) {
        int yep = 1;
        for (int i = 2; i <= 20; i++) {
            if (a % i != 0) {
                yep = 0;
                break;
            }
        }
        if (yep) {
            printf("%d", a);
            return 0;
        }
        a += 20;
    }
}