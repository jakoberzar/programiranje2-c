#include <stdio.h>

int main() {
    FILE *f = fopen("problem11in", "r");
    int grid[20][20] = {};
    int countread = 0;
    while (!feof(f)) {
        int d;
        fscanf(f, "%d ", &d);
        grid[countread / 20][countread % 20] = d;
        countread++;
    }
    printf("\n");
    for (int i = 0; i < 20; i++) {
        for (int j = 0; j < 20; j++) {
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }
    
    long max = 0;
    // check horizontal
    printf("h\n");
    for (int i = 0; i < 20; i++) {
        for (int j = 3; j < 20; j++) {
            long my = (long)((long)(grid[i][j-3] * grid[i][j-2]) * (long)(grid[i][j-1] * grid[i][j]));
            if (my > max) {
                max = my;
                printf("i: %d j: %d, res: %ld\n", i, j, my);
            }
        }
    }
    // check vertical
    printf("v\n");
    for (int i = 0; i < 20; i++) {
        for (int j = 3; j < 20; j++) {
            long my = (long)((long)(grid[j-3][i] * grid[j-2][i]) * (long)(grid[j-1][i] * grid[j][i]));
            if (my > max) {
                max = my;
                printf("i: %d j: %d, res: %ld\n", i, j, my);
            }
        }
    }
    // check diagonal
    printf("d\n");
    for (int i = 3; i < 20; i++) {
        for (int j = 3; j < 20; j++) {
            long my = (long)((long)(grid[i-3][j-3] * grid[i-2][j-2]) * (long)(grid[i-1][j-1] * grid[i][j]));
            if (my > max) {
                max = my;
                printf("i: %d j: %d, res: %ld\n", i, j, my);
            }
        }
    }
    
    // check diagonal
    printf("d\n");
    for (int i = 0; i < 17; i++) {
        for (int j = 3; j < 20; j++) {
            long my = (long)((long)(grid[i+3][j-3] * grid[i+2][j-2]) * (long)(grid[i+1][j-1] * grid[i][j]));
            if (my > max) {
                max = my;
                printf("i: %d j: %d, res: %ld\n", i, j, my);
            }
        }
    }
    printf("%ld\n",max);
    return 0;
}