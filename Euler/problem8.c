#include <stdio.h>
#include <ctype.h>

int main() {
    long max = 0;
    char d;
    char d2;
    char prev[16] = {};
    int a;
    FILE *f = fopen("problem8in", "r");
    while (a = fscanf(f, "%c ", &d) && !(d == '\n' && d2 == '\n')) {
        if (feof(f)) break;
        //printf("a: %d\n", a);
        if (isdigit(d)) {
            for (int i = 0; i < 12; i++) {
                prev[i] = prev[i+1];
            }
            prev[12] = d - '0';
            long mul = 1;
            for (int i = 0; i < 13; i++) {
                mul *= prev[i];
            }
            if (mul > max) {
                max = mul;
                //printf("Max: %d * %d * %d * %d = %d\n", prev[0], prev[1], prev[2], prev[3], mul);
            }
        }
        d2 = d;
    } 
    printf("%ld\n", max);
}