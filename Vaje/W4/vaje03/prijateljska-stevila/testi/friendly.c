#include <stdio.h>
#include <math.h>
int sumDividers(int n);

int main() {
    int n;
    scanf("%d", &n);
    int div = sumDividers(n);
    if (sumDividers(div) == n) {
        printf("%d", div);
    } else {
        printf("NIMA");
    }
    return 0;
}

int sumDividers(int n) {
    int sum = 0;
    int root = (int)(floor(sqrt(n)));
    for (int i = 1; i < root; i++) {
        if (n % i == 0) {
            sum += i;
            sum += (i != 1) ? n / i : 0; // če je 1 ne rabm delit
        }
    }
    return sum;
}