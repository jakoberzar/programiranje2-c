#include <stdio.h>
int collatzLen(int n);
int cachedLen(int n);
int cache[10000];

int main() {
    int n = 0;
    int maxI = 1;
    int maxVal = 1;
    cache[1] = 1;
    scanf("%d", &n);
    for (int i = 0; i <= n; i++) {
        int col = collatzLen(i);
        if (i < 10000) cache[i] = col;
        if (col > maxVal) {
            maxI = i;
            maxVal = col;
            //printf("%d %d\n", maxI, maxVal);
        }
    }
    printf("%d %d", maxI, maxVal);
    return 0;
}

int collatzLen(int n) {
    int counter = 1;
    int cur = n;
    while (cur > 1) {
        if (cur % 2 == 0) cur /= 2;
        else cur = cur * 3 + 1;
        int cached = cachedLen(cur);
        if (cached == 1) counter++;
        else return counter + cached;
    }
    return counter;
}

int cachedLen(int n) {
    if (n < 10000) {
        if (cache[n] == 0) return 1;
        return cache[n];
    } else {
        return 1;
    }
    /*
    switch(n) {
        case 3: 
            return 8;
        case 6:
            return 0;
        case 7:
            return 17;
        case 9:
            return 20;
        case 18:
            return 21;
        case 25:
            return 24;
        case 27:
            return 112;
        case 33:
            return 120;
        case 66:
            return 121;
        case 129:
            return 122;
        case 171:
            return 125;
        case 231:
            return 128;
        case 313:
            return 131;
        default: return 1;
    }*/
}