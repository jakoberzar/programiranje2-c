#include <stdio.h>
#include <stdlib.h>
struct _kopica {
    int value;
    struct _kopica *left;
    struct _kopica *right;
};
typedef struct _kopica kopica;
kopica *globalna;

kopica* merge(kopica* a, kopica* b);
void push(int n);
int pop();

int main() {
    int n;
    globalna = malloc(sizeof(kopica));
    globalna->value = -1;
    globalna->left = NULL;
    globalna->right = NULL;
    scanf("%d", &n);
    globalna->value = n;
    
    while (scanf("%d", &n) != EOF) {
        push(n);    
    }
    
    int count = 0;
    //printf("%d ", n);
    n = pop(globalna);
    while (globalna) {
        //printf("%d levo:%d, desno: %d\n", n, globalna->left->value, globalna->right->value);
        printf("%d ", n);
        n = pop();
        count++;
    }
    printf("%d ", n);
    return 0;
}

kopica* merge(kopica* a, kopica* b) {
    if (a == NULL) {
        return b;
    }
    if (b == NULL) {
        return a;
    }
    kopica *manjsa;
    kopica *vecja;
    manjsa = a->value >= b->value ? b : a;
    vecja = a->value < b->value ? b : a;
    /*kopica *c = malloc(sizeof(kopica));
    c->value = manjsa->value;
    c->right = manjsa->left;
    c->left = merge(vecja, manjsa->right);
    free(manjsa);
    return c;*/
    kopica* t = manjsa->right;
    manjsa->right = manjsa->left;
    manjsa->left = merge(t, vecja);
    /*if (manjsa->left && manjsa->right) 
    printf("merged to: %d, left: %d, right: %d\n", manjsa->value, manjsa->left->value, manjsa->right->value);*/
    return manjsa;
}

void push(int n) {
    kopica *nova = malloc(sizeof(kopica));
    nova->value = n;
    nova->left = NULL;
    nova->right = NULL;
    globalna = merge(globalna, nova);
}

int pop() {
    int val = globalna->value;
    //if (k->left == NULL && k->right == NULL) return -1;
    //printf("test\n");
    kopica *old = globalna;
    globalna = merge(globalna->left, globalna->right);
    free(old);
    return val;
}
