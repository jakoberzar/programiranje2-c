#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void insert_sort(int *a, int n, char **c);
int main() {
    int n, k;
    scanf("%d %d\n", &n, &k);
    char *imena[n];
    int knjige[n];
    
    for (int i = 0; i < n; i++) {
        imena[i] = (char *)(malloc(sizeof(char) * 17));
        scanf("%s\n", imena[i]);
    }
    
    for (int i = 0; i < n; i++) {
        scanf("%d\n", &knjige[i]);
    }
    
    insert_sort(knjige, n, imena);
    // insert_sort
    /*for (int i = 1; i < n; i++) {
        int tmp = knjige[i];
        char tmpC[16]; strcpy (tmpC, imena[i]);
        int j = i;
        while ((j > 0) && (tmp > knjige[j-1])) {
            knjige[j] = knjige[j-1]; 
            strcpy(imena[j], imena[j-1]);
            //imena[j] = imena[j-1]; 
            j--;
        }
        knjige[j] = tmp;
        //imena[j] = tmpC;
        strcpy(imena[j], tmpC);
    }*/
    
    
    int place = 0;
    int prev = -1;
    int prevstreak = 0;
    for (int i = 0; i < n; i++) {
        //printf("prev: %d, knjige: %d, %d\n", prev,knjige[i], prev == knjige[i]);
        if (prev == knjige[i]) {
            prevstreak++;
        } else {
            place += prevstreak + 1;
            prevstreak = 0;
        }
        if (place > k) break;
        printf("%d. %s (%d)\n", place, imena[i], knjige[i]);
        prev = knjige[i];
    }
    return 0;
}

void insert_sort(int *a, int n, char **c) {
    for (int i = 1; i < n; i++) {
        int tmp = a[i];
        char *tmpC = malloc(17 * sizeof(char));
        strcpy(tmpC, c[i]);
        int j = i;
        while ((j > 0) && (tmp < a[j-1])) {
            a[j] = a[j-1];
            strcpy(c[j], c[j-1]);
            j--;
        }
        a[j] = tmp;
        strcpy(c[j], tmpC);
        free(tmpC);
    }
}