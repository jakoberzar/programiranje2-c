#include <stdio.h>
#include <stdlib.h>
// Arraylist vs Linked list - če moramo brisat / dodajat nek vmes, je treba mnj delat kt bi blo treba drgač

struct vagon {
    int vagonID;
    int teza;
    struct vagon* next;
};
typedef struct vagon vagon;
struct vlak {
    int vlakID;
    vagon* firstVagon;
    int teza;
    struct vlak* next;
};
typedef struct vlak vlak;

void priklopi(vlak *firstVlak, int vlakId, int vagonId, int teza);
vlak* getVlak(vlak* firstVlak, int id);
vagon* getVagon(vagon* firstVagon, int id);
vagon* zadnjiVagon(vlak* vlak);

int main() {
    vlak* firstVlak;
    firstVlak = NULL;
    
    char u;
    int vlakID, vagonID, teza;
    while (scanf(" (%c, %d, %d, %d)", &u, &vlakID, &vagonID, &teza) != EOF) {
        printf("%c - vlak %d vagon %d teza %d\n", u, vlakID, vagonID, teza);
        if (u == 'P') {
            priklopi(firstVlak, vlakID, vagonID, teza);
        }
    }
    
    vlak* vlaken = firstVlak;
    while (vlaken != NULL) {
        printf("test");
        printf("%d teza: %d\n", vlaken->vlakID, vlaken->teza);
        vlaken = vlaken->next;
    }
    return 0;
}

void priklopi(vlak *firstVlak, int vlakId, int vagonId, int teza) {
    vlak *v = getVlak(firstVlak, vlakId);
    v->teza += teza;
    vagon* zadnji = zadnjiVagon(v);
    zadnji = malloc(sizeof(vagon));
    zadnji->vagonID = vagonId;
    zadnji->teza = teza;
    //printf("came: vlak %dt%d v%d, vagon %d %d\n", v->vlakID, v->teza, v->firstVagon->vagonID, zadnji->vagonID, zadnji->teza);
}

vlak* getVlak(vlak* firstVlak, int id) {
    vlak* trenutni = firstVlak;
    vlak* prejsnji = NULL;
    while (trenutni != NULL && trenutni->vlakID != id) {
        prejsnji = trenutni;
        trenutni = trenutni->next;
    }
    if (trenutni == NULL || trenutni->vlakID != id) {
        trenutni = malloc(sizeof(vlak));
        trenutni->vlakID = id;
        trenutni->teza = 0;
        trenutni->firstVagon = NULL;
        if (prejsnji != NULL) {
            trenutni->next = prejsnji->next;
            prejsnji->next = trenutni;
        } else {
            trenutni->next = firstVlak;
            firstVlak = trenutni;
        }
    }
    return trenutni;
}

vagon* getVagon(vagon* firstVagon, int id) {
    vagon* trenutni = firstVagon;
    while(trenutni != NULL) {
        if (trenutni->vagonID != id) trenutni = trenutni->next;
    }
    if (trenutni == NULL) {
        trenutni = malloc(sizeof(vagon));
        trenutni->vagonID = id;
    }
    return trenutni;
}

vagon* zadnjiVagon(vlak* vlak) {
    vagon* prejsnji = NULL;
    vagon* trenutni = vlak->firstVagon;
    while (trenutni != NULL) {
        prejsnji = trenutni;
        trenutni = trenutni->next;
    }
    if (prejsnji != NULL) {
        trenutni->next = prejsnji->next;
        prejsnji->next = trenutni;
    } else {
        trenutni->next = vlak->firstVagon;
        vlak->firstVagon = trenutni;
    }
    
    return trenutni;
}