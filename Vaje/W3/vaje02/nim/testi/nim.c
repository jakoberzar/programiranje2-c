#include <stdio.h>
#include <stdlib.h>
int vzemi(int preostalih, int k);
int izvedi(int koliko, int preostalih, int player);

int main() {
	int seed, k, preostalih;
	scanf("%d", &seed);
	scanf("%d %d", &k, &preostalih);
	srand(seed);

	int num;
	while (scanf("%d", &num) == 1 && preostalih > 0) {
		preostalih = izvedi(num, preostalih, 0);
		if (preostalih > 0) {
			int v = vzemi(preostalih, k);
			preostalih = izvedi(v, preostalih, 1);
		}
	}
	return 0;
}

int vzemi(int preostalih, int k) {
	int v = rand() % k + 1;
	return preostalih > v ? v : preostalih;
}

int izvedi(int koliko, int preostalih, int player) {
	int newPreostalih = preostalih - koliko; // but what if -?
	
	printf("%d - %d = %d\n", preostalih, koliko, newPreostalih);
	if (newPreostalih == 0) {
		if (player == 1) printf("IGRALEC\n"); // Lihk kontra logika cause kdo zmaga
		else printf("RACUNALNIK\n");
	}
	return newPreostalih;
}
