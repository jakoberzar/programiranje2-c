#include <stdio.h>
#include <math.h>
int jePrastevilo(int n);
int main() {
	int n;
	scanf("%d", &n);
	int i = 0;
	for (i = 3; i <= (int)(floor(sqrt(n))); i++) {
		if (n % i == 0 && jePrastevilo(i) && n / i != i && jePrastevilo (n/i)) {
			printf("D\n");
			return 0;
		}
	}
	printf("N\n");
	return 0;
}

int jePrastevilo(int n) {
	int i;
	for (i = 2; i < (int)(floor(sqrt(n))); i++) {
		if (n % i == 0) return 0;
	}
	return 1;
}
