#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
	int n, k;	
	scanf("%d %d", &n, &k);
	char *imena[n]; //tabela pointerjev na stringe
	int stevila[n]; //tabela stevilk (obviously)
	

	//BRANJE
	//strlen(string) - vrne dolžino stringa
	//strcpy(st1, st2) - kopira string st2 v st1
	for (int i = 0; i < n; i++) {
		char niz[17]; //string dolzine 17, ker je to najdaljše možno ime
		scanf("%s", niz); //zapišemo ime v začasni niz
		
		//alociramo prostor za ime pri 'i'
		imena[i] = malloc(sizeof(char) * strlen(niz) + 1);
		strcpy(imena[i], niz);
	}
	
	for (int i = 0; i < n; i++){
		scanf("%d", &stevila[i]);
	}

	//UREJANJE
	//bubble sort
	for (int i = 0; i < n - 1; i++) {
		for (int j = 0; j < n - i - 1; j++) {
			if (stevila[j] < stevila[j + 1]) {
				char *niz = imena[j];
				imena[j] = imena[j+1];
				imena[j + 1] = niz;
				int stevilo = stevila[j];
				stevila[j] = stevila[j+1];
				stevila[j + 1] = stevilo;			
			}
			
		}
	}

	//IZPISOVANJE
	//ii - števec
	for (int i = 0, ii = 1; (i < n && i < k) || (i < n && stevila[i] == stevila[i-1]); i++){
		if(stevila[i] != stevila[i-1]) ii = i+1;		
		printf("%d. %s (%d)\n", ii, imena[i], stevila[i]);
	}
}




