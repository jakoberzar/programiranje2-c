#include <stdio.h>
int main() {
    int n;
    scanf("%d\n", &n);
    int arr[n];
    for (int i = 0; i < n; i++) {
        scanf("%d ", &arr[i]);
    }
    
    int maxIndex = 0;
    long maxVsota = arr[0];
    int maxDolzina = 1;
    
    /*for (int i = 0; i < n; i++) { // i = index
        long vsota = 0;
        for (int j = i; j < n; j++) {
            vsota += arr[j];
            if (vsota > maxVsota) {
                maxIndex = i;
                maxVsota = vsota;
                maxDolzina = j - i + 1;
            }
        }
    }*/
    
    long vsota = 0;
    int start = 0;
    for (int i = 0; i < n; i++) {
        vsota += arr[i];
        if (vsota > maxVsota) {
            maxVsota = vsota;
            maxIndex = start;
            maxDolzina = i - start + 1;
        } else if (vsota < 0) {
            vsota = 0;
            start = i + 1;
        }
    }
    /*for (int i = 1; i <= n; i++) { // i = dolzina
        for (int j = 0; j <= n - i; j++) { // j = index
            long vsota = 0;
            for (int k = 0; k < i; k++) {
                vsota += arr[j + k];
            }
            if (vsota > maxVsota || (maxVsota == vsota && j < maxIndex) ) {
                maxIndex = j;
                maxVsota = vsota;
                maxDolzina = i;
            }
        }
    }*/
    printf("%d %d %ld", maxIndex, maxDolzina, maxVsota);
    return 0;
}