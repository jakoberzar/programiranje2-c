#include <stdio.h>
int main() {
	int k, h, m;
	scanf("%d %d %d", &k, &h, &m);
	int combm = h * 60 + m;
	int combstation = 5 * 60 + 0;
	for(int i = combstation; i <= combm; i += k) {
		combstation = i;
		//printf("%d:%d\n", i / 60, i % 60);
	}
	combstation += k;
	printf("%d", combstation - combm);
	return 0;
}
