#include <stdio.h>
int rekf(int *vedra, int vedran, int *vedracap, int depth, int seek);
int main() {
    int n, seeker;
    scanf("%d\n", &n);
    int vedra[n];
    for (int i = 0; i < n; i++) {
        scanf("%d ", &vedra[i]);
    }
    scanf("\n%d", &seeker);
    rekf(vedra, n, vedra, 0, 30);
    return 0;
}

int rekf(int *vedra, int vedran, int *vedracap, int depth, int seek) {
    int lowest;
    // Našel rešitev?
    for (int i = 0; i < vedran; i++) {
        if (vedra[i] == seek) return depth;
    }
    
    if (depth > 15) {
        return -1; // Too deep! (That's what she said!)
    }
    
    for (int i = 0; i < vedran; i++) {
        // Gremo skoz vsa vedra
        
        // Dodamo vse notr
        for (int j = 0; j < n; j++) {
            if (vedra[i] + vedracap[j] <= vedracap[i]) {
                vedra[i] += vedracap[j];
                int res = rekf(vedra, vedran, vedracap, depth, seek);
                if (res > 0) return res;
            }
        }
    }
}