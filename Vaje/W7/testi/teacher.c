#include <stdio.h>

int rekurzija(int n, int vFull[], int v[], int x, int kToGo) {
    // preveri uspeh
    for (int i = 0; i < n; i++) {
        if (v[i] == x) return kToGo;
    }
    
    // preveri neuspeh
    if (kToGo < 0) {
        return kToGo;
    }
    
    // GENERIRAJ nova stanja
    // prelivanja
    for (int i = 0; i < n + 1; i++) {
        for (int j = 0; j < n + 1; j++) {
            if (i != j && v[i] > 0 && v[j] < vFull[j]) {
                // shrani stanje
                int vi = v[i];
                int vj = v[j];
                if (vFull[j] - v[j] >= v[i]) {
                    v[j] = [j] + v[i];
                    v[i] = 0;
                } else {
                    v[i] = v[i] - v[j];
                    v[j] = vFull[j];
                }
                int retValue = rekurzija(n, vFull, v, x, kToGo - 1);
                v[i] = vi;
                v[j] = vj;
                if (retValue >= 0) {
                    return retValue;
                }
                
            }
        }
    }
    
    // vrni neuspeh
    return -1;
}

int main() {
    int n;
    scanf("%d", n);
    int vFull[n + 1];
    int v[n + 1];
    int vFullSum = 0;
    for (int i = 0; i < n; i++) {
        scanf("%d ", &vFull[i]);
        v[i] = 0; // inicializiraj začetno stanje
        vFullSum += vFull[i];
    }
    vFull[n] = vFullSum;
    v[n] = vFullSum;
    int x;
    scanf("%d", &x);
    
    // omejene rekurzije
    for (int i = 0; ; i++) {
        int retValue = rekurzija(n, vFull, v, x, i);
        if (retValue >= 0) {
            printf("%d\n", i);
            break;
        }
    }
}