#include <stdio.h>
char f(int v) {
    char Z[] = {' ', '.', '\'', ':', 'o', '&', '8', '#', '@'};
    int S[] = {230, 200, 180, 160, 130, 100, 70, 50};
    for (int i = 0; i < 8; i++) {
        if (v >= S[i]) return Z[i];
    }
    return Z[8];
    
}

int main() {
    char fileInName[1024];
    char fileOutName[1024];
    int n;
    scanf("%1023s %1023s %d", fileInName, fileOutName, &n);
    FILE *inputFile = fopen(fileInName, "r");
    FILE *outputFile = fopen(fileOutName, "w");
    if (!inputFile || !outputFile) return 1;
    
    int w, h;
    fscanf(inputFile, "P6 %d %d 255\n", &w, &h);
    for (int v = 0; v < h / n; v++) {
        int t[w/n];
        for (int i = 0; i < w / n; i++) {
            t[i] = 0;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < w; j++) {
                unsigned char r, g, b;
                fscanf(inputFile, "%c%c%c", &r, &g, &b);
                t[j/n] += (r+g+b);
                //printf("%d", r+g+b);
            }
            //printf("\n");
        }
        
        for (int i = 0; i < w / n; i++) {
            char c = f(t[i] / (n * n * 3));
            fprintf(outputFile, "%c", c);
        }
        fprintf(outputFile, "\n");
    }
    fclose(outputFile);
    
    return 0;
}