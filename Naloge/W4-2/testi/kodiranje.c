#include <stdio.h>
int kodiraj(int n);
int dekodiraj(int n);

int main() {
    int mode, n;
    scanf("%d %d\n", &mode, &n);
    if (mode == 1) kodiraj(n);
    else dekodiraj(n);
    if (n == 0) printf("\n");
    return 0;
}

int kodiraj(int n) {
    int prev = -1;
    int count = 0;
    for (int i = 0; i < n; i++) {
        int cur;
        scanf("%d ", &cur);
        if (prev == cur) count++;
        else {
            if (prev != -1) printf("%d %d ", count, prev);
            count = 1;
            prev = cur;
        }
    }
    if (prev != -1) printf("%d %d ", count, prev);
    return 0;
}

int dekodiraj(int n) {
    for (int i = 0; i < n; i++) {
        int count, st;
        scanf("%d %d", &count, &st);
        for (int j = 0; j < count; j++) {
            printf("%d ", st);
        }
    }
    return 0;
}