#include <stdio.h>
#include <stdlib.h>
struct _kopica {
    int value;
    struct _kopica *left;
    struct _kopica *right;
};
typedef struct _kopica kopica;
kopica *globalna;

kopica* merge(kopica* a, kopica* b);
void push(int n);
int pop();
void izpisiKopico(kopica* root);
int main() {
    int n;
    globalna = malloc(sizeof(kopica));
    globalna->value = -1;
    globalna->left = NULL;
    globalna->right = NULL;
    scanf("%d", &n);
    globalna->value = n;
    
    while (scanf("%d", &n) != EOF) {
        push(n);    
    }
    
    izpisiKopico(globalna);
    /*
    int count = 0;
    n = pop(globalna);
    while (globalna) {
        printf("%d ", n);
        n = pop();
        count++;
    }
    printf("%d ", n);*/
    return 0;
}

kopica* merge(kopica* a, kopica* b) {
    if (a == NULL) {
        return b;
    }
    if (b == NULL) {
        return a;
    }
    kopica *manjsa;
    kopica *vecja;
    manjsa = a->value >= b->value ? b : a;
    vecja = a->value < b->value ? b : a;
    kopica *c = malloc(sizeof(kopica));
    c->value = manjsa->value;
    c->right = manjsa->left;
    c->left = merge(vecja, manjsa->right);
    free(manjsa);
    return c;
    /* kopica* t = manjsa->right;
    manjsa->right = manjsa->left;
    manjsa->left = merge(t, vecja);
    return manjsa;*/
}

void push(int n) {
    kopica *nova = malloc(sizeof(kopica));
    nova->value = n;
    nova->left = NULL;
    nova->right = NULL;
    globalna = merge(globalna, nova);
}

int pop() {
    int val = globalna->value;
    kopica *old = globalna;
    globalna = merge(globalna->left, globalna->right);
    free(old);
    return val;
}

void izpisiKopico(kopica* root) {
    if (root == NULL) return;
    izpisiKopico(root->left);
    printf("%d ", root->value);
    izpisiKopico(root->right);
}