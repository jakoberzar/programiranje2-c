#include <stdio.h>
#include <stdlib.h>
char **vrstice;
int znakov;
int *toarr (unsigned long a) {
    int *arr = malloc(8 * sizeof(int));
    unsigned long b = a;
    for (int i = 7; i >= 0; i--) {
        arr[i] = b % 256;
        b /= 256;
    }
    return arr;
}

char *tozvezde(int a) {
    char *arr = malloc(8 * sizeof(char));
    int b = a;
    for (int i = 7; i >= 0; i--) {
        int r = b % 2;
        if (r) arr[i] = '*';
        else arr[i] = ' ';
        b /= 2;
    }
    return arr;
}


int main() {
    char outname[100+1];
    scanf("%s", outname);
    int n;
    scanf("%d", &n);
    FILE *f = fopen(outname, "w");
    if (n == 1) {
        int arr[8];
        for (int i = 0; i < 8; i++) {
            scanf("%d", &(arr[i]));
        }
        for (int i = 0; i < 8; i++) {
            char *a = tozvezde(arr[i]);
            fprintf(f, "%s\n", a);
            free(a);
        }
    } else {
        int **vrstice = malloc(sizeof(int *));
        int poop;
        int num = 0;
        do {
            unsigned long a;
            scanf("%lu", &a);
            num++;
            int *crka = toarr(a);
            vrstice = realloc(vrstice, num * sizeof(int*));
            vrstice[num - 1] = crka;
            
        } while(scanf("%d", &poop) != EOF);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < num; j++) {
                char *s = tozvezde(vrstice[j][i]);;
                fprintf(f, "%s\t\t\t", s);
                free(s);
            }
            fprintf(f, "\n");
        }
        for (int i = 0; i < num; i++) free(vrstice[i]);
        free(vrstice);
    }
    fclose(f);
}