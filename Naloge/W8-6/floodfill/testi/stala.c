#include <stdio.h>
void fill(int x, int y, int w, int h, int n, int *t);
int main() {
    int w, h;
    int n;
    scanf("%d %d", &w, &h);
    scanf("%d", &n);
    int polje[w][h];
    int crte[n][4];
    for (int i = 0; i < n; i++) {
        int *crta = &(crte[i][0]);
        scanf("%d %d %d %d", &crta[0], &crta[1], &crta[2], &crta[3]);
    }
    int *p = &(polje[0][0]);
    for (int i = 0; i < w * h; i++) {
        p[i] = -1;
    }
    
    for (int i = 0; i < n; i++) {
        int *crta = &(crte[i][0]);
        for (int x = crta[0]; x <= crta[2]; x++) {
            polje[x][crta[1]] = 0;
        }
        for (int y = crta[1]; y <= crta[3]; y++) {
            polje[crta[0]][y] = 0;
        }
    }
    
    int count = 0;
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            if (polje[i][j] == -1) {
                count++;
                fill(i, j, w, h, count, p);
            }
        }
    }/*
    // Dump
    for (int i = 0; i < h; i++) {
        for (int j  = 0; j < w; j++) {
            //printf("%d ", polje[j][i]);
            printf("%d ", p[j * w + i]);
        }
        printf("\n");
    }*/
    printf("%d", count);
}

void fill(int x, int y, int w, int h, int n, int *t) {
    if (x >= 0 && y >= 0 && x < w && y < h && t[x * w + y] == -1) {
        t[x * w + y] = n;
        fill(x + 1, y, w, h, n, t);
        fill(x - 1, y, w, h, n, t);
        fill(x, y + 1, w, h, n, t);
        fill(x, y - 1, w, h, n, t);
    }
}