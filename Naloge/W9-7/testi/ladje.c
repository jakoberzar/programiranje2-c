#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct _plovba {
    char zacetek[100+1];
    char konec[100+1];
    int trajanje;
    int gor;
    int dol;
    struct _plovba *next;
} plovba;
typedef struct _dan {
    int n;
    struct _dan *next;
} dan;
void dump(plovba *ship);
plovba* prva;
plovba** urejene;
dan* prvi;
int stPlovb;
int main() {
    // reads
    plovba *prejsnja = NULL;
    stPlovb = 0;
    while (1) {
        plovba* nova = malloc(sizeof(plovba));
        scanf("%100s", nova->zacetek);
        if (strcmp(nova->zacetek, "?") == 0) {
            free(nova);
            break;
        }
        scanf("%100s", nova->konec);
        scanf("%d %d %d", &(nova->trajanje), &(nova->gor), &(nova->dol));
        nova->next = NULL;
        if (prejsnja) {
            prejsnja->next = nova;
        } else {
            prva = nova;
        }
        prejsnja = nova;
        stPlovb++;
    }
    int n;
    dan *prejsnji = NULL;
    while (scanf("%d ", &n) != EOF) {
        dan *nov = malloc(sizeof(dan));
        nov->n = n;
        nov->next = NULL;
        if (prejsnji) {
            prejsnji->next = nov;
        } else {
            prvi = nov;
        }
        prejsnji = nov;
    }
    
    
    urejene = malloc(stPlovb * sizeof(plovba *));
    
    // find first
    plovba* trenZunanja = prva;
    while (trenZunanja) {
        int notFound = 1;
        plovba* trenNotranja = prva;
        while (trenNotranja && notFound) {
            if (strcmp(trenNotranja->konec, trenZunanja->zacetek) == 0) {
                notFound = 0;
            }
            trenNotranja = trenNotranja->next;
        }
        if (notFound) {
            urejene[0] = trenZunanja;
            break;
        }
        trenZunanja = trenZunanja->next;
    }
    
    for (int i = 1; i < stPlovb; i++) {
        char *konec = urejene[i - 1]->konec;
        plovba* trenutna = prva;
        while (trenutna) {
            if (strcmp(konec, trenutna->zacetek) == 0) {
                break;
            }
            trenutna = trenutna->next;  
        }
        if (trenutna) {
            urejene[i] = trenutna;
        }
    }
    
    dan *trenutni = prvi;
    int zdj = 1;
    int potniki = 0;
    for (int i = 0; i < stPlovb; i++) {
        potniki += urejene[i]->gor;
        while (trenutni && trenutni->n < (zdj + urejene[i]->trajanje)) {
            printf("%d %s - %s %d\n", trenutni->n, urejene[i]->zacetek, urejene[i]->konec, potniki);
            trenutni = trenutni->next;
        }
        potniki -= urejene[i]->dol;
        zdj += urejene[i]->trajanje;
    }
    while (trenutni) {
        printf("%d NAPAKA\n", trenutni->n);
        trenutni = trenutni->next;
    }
    
}

void dump(plovba *ship) {
    printf("%s - %s; %d, %d, %d\n", ship->zacetek, ship->konec, ship->trajanje, ship->gor, ship->dol);
}