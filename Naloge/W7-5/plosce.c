#include <stdio.h>
int count_stacks(int *xs, int *ys, int n);
int rekturzion(int *xs, int *ys, int n, int x, int y);
int main() {
    int n;
    scanf("%d\n", &n);
    int xs[n], ys[n];
    for (int i = 0; i < n; i++) {
        scanf("%d %d\n", &xs[i], &ys[i]);
    }
    int maxStack = 0;
    
    // Iterativna rešitev
    maxStack = count_stacks(xs, ys, n);
    
    /*
    // Rešitev z rekurzijo
    for (int i = 0; i < n; i++) {
        int r = rekturzion(xs, ys, n, xs[i], ys[i]);
        if (r > maxStack) maxStack = r;
    }*/
    printf("%d\n", maxStack);
    return 0;
}

int rekturzion(int *xs, int *ys, int n, int x, int y) {
    int max = 0;
    for (int i = 0; i < n; i++) {
        if ((xs[i] < x && ys[i] < y) || (xs[i] < y && ys[i] < x)) {
            int r = rekturzion(xs, ys, n, xs[i], ys[i]);
            if (r > max) {
                max = r;
            }
        }
    }
    return max + 1;
}

int count_stacks(int *xs, int *ys, int n) {
    int appropriates[n];
    for (int i = 0; i < n; i++) {
        int count = 0;
        for (int j = 0; j < n; j++) {
            int obrnjena = xs[i] < ys[j] && ys[i] < xs[j];
            if ((xs[i] < xs[j] && ys[i] < ys[j]) || obrnjena) {
                count++;
            }
        }
        appropriates[i] = count;
    }
    int connections[n];
    for (int i = 0; i < n; i++) {
        int maxcon = -1;
        int maxi = -1;
        for (int j = 0; j < n; j++) {
            int obrnjena = xs[i] < ys[j] && ys[i] < xs[j];
            if (((xs[i] < xs[j] && ys[i] < ys[j]) || obrnjena) && appropriates[j] > maxcon) {
                maxcon = appropriates[j];
                maxi = j;
            }
        }
        connections[i] = maxi;
    }
    int maxcount = 1;
    for (int i = 0; i < n; i++) {
        int count = 1;
        int next = connections[i];
        while (next != -1) {
            count++;
            next = connections[next];
        }
        if (count > maxcount) maxcount = count;
    }
    return maxcount;
}