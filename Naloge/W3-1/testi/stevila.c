#include <stdio.h>

long obrni(long a);
int stStevk(long a);
long pow10(long a);

int main() {
    long a, b;
    scanf("%ld %ld", &a, &b);
    
    long obrnjenB = obrni(b);
    int mesto = stStevk(a) - 1;
    while (obrnjenB > 0) {
        long i, n;
        n = 0;
        for (i = 0; i < obrnjenB % 10; i++) {
            n *= 10;
            long digit = a / pow10(mesto);
            n += digit;
            a -= digit * pow10(mesto);
            //printf("Digit: %l, a: %l, n: %l\n", digit, a, n);
            mesto--;
        }
        printf("%ld\n", n);
        obrnjenB /= 10;
    }
    
    return 0;
}

long obrni(long a) {
    long novo = 0;
    long preostalo = a;
    while (preostalo > 0) {
        novo *= 10;
        novo += preostalo % 10;
        preostalo /= 10;
    }
    return novo;
}

int stStevk(long a) {
    int stevk = 1;
    long stevilo = a;
    while (stevilo / 10 > 0) {
        stevk++;
        stevilo /= 10;
    }
    return stevk;
}

long pow10(long a) {
    long st = 1;
    for (int i = 0; i < a; i++) {
        st *= 10;
    }
    return st;
}