#include <stdio.h>
int main() {
    int n;
    scanf("%d\n", &n);
    int vals[n];
    for (int i = 0; i < n; i++) {
        scanf("%d ", &vals[i]);
    }
    for (int i = 0; i < n; i++) {
        if (vals[i] % 2 == 0) {
            int min = i;
            for (int j = i + 1; j < n; j++) {
                if (vals[j] % 2 == 0 && vals[j] < vals[min]) {
                    min = j;
                }
            }
            int tmp = vals[i];
            vals[i] = vals[min];
            vals[min] = tmp;
        }
    }
    for (int i = 0; i < n; i++) {
        printf("%d ", vals[i]);
    }
    return 0;
}