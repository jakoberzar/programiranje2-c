#include <stdio.h>
#include <stdlib.h>

int main() {
    // array initially empty
    int *kacax = NULL;
    int *kacay = NULL;
    
    int kacalen = 1;
    int smer = 0; // 0 sever, 1 vzhod, 2 jug, 3 zahod
    
    int *px = NULL;
    int *py = NULL;
    int *pt = NULL;
    int pl = 0;
    
    int korakreq;
    // Read the data
    scanf("%d\n", &pl);
    px = realloc( px, pl * sizeof *px );
    py = realloc( py, pl * sizeof *py );
    pt = realloc( pt, pl * sizeof *pt );
    for (int i = 0; i < pl; i++) {
        scanf("%d %d %d\n", &(px[i]), &(py[i]), &(pt[i]));
    }
    scanf("%d", &korakreq);
    
    // change the size of the array
    kacax = realloc( kacax, kacalen * sizeof *kacax );
    kacay = realloc( kacay, kacalen * sizeof *kacay );
    
    int kpovecaj = 0;
    for (int korak = 1; korak <= korakreq; korak++) {
        // Premakni stvari naprej!
        int cachedx = kacax[0];
        int cachedy = kacay[0];
        for (int kos = 0; kos < kacalen; kos++) {
            if (kos == 0) {
                int newx = kacax[0];
                int newy = kacay[0];
                if (smer == 0) newy++;
                if (smer == 1) newx++;
                if (smer == 2) newy--;
                if (smer == 3) newx--;
                for (int kosif = 0; kosif < kacalen; kosif++) {
                    if (kacax[kosif] == newx && kacay[kosif] == newy) {
                        printf("0");
                        return 0;
                    }
                }
                
                //printf("new %d %d\n", newx, newy);
                for (int predmet = 0; predmet < pl; predmet++) {
                    if (px[predmet] == newx && py[predmet] == newy && pt[predmet] != 0) {
                        //printf("predmet %d %d t%d\n", newx, newy, pt[predmet]);
                        if (pt[predmet] == 1) kpovecaj++;
                        if (pt[predmet] == 2) smer--;
                        if (pt[predmet] == 3) smer++;
                        pt[predmet] = 0;
                        if (smer < 0) smer += 4;
                        if (smer > 3) smer -= 4;
                    }
                }
                kacax[0] = newx;
                kacay[0] = newy;
                
            } else {
                int cx = cachedx;
                int cy = cachedy;
                cachedx = kacax[kos];
                cachedy = kacay[kos];
                kacax[kos] = cx;
                kacay[kos] = cy;
            }
        }
        
        if (kpovecaj) {
            //printf("increase!\n");
            kacalen++;
            kacax = realloc( kacax, kacalen * sizeof *kacax );
            kacay = realloc( kacay, kacalen * sizeof *kacay );
            kpovecaj = 0;
        }
        
        
    } 
    
    printf("%d", kacalen);
    return 0;
}