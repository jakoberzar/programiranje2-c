#include <stdio.h>
#include <stdlib.h>
int pc = 0;
double *stack;

void push(double d) {
    stack[pc] = d;
    pc++;
}

double pop() {
    pc--;
    return pc + 1 ? stack[pc] : -1;
}

double peek() {
    return pc ? stack[pc - 1] : -1;
}

int operate(char op) {
    double s = stack[0];
    for (int i = 1; i < pc; i++) {
        if (op == '+') {
            s += stack[i];
        } else if (op == '-') {
            s -= stack[i];
        } else if (op == '*') {
            s *= stack[i];
        } else if (op == '/') {
            s /= stack[i];
        }
    }
    pc = 0;
    push(s);
    return 0;
}

int main() {
    stack = (double*)calloc(4, sizeof(double));
    double t;
    char op;
    int read;
    read = 1;
    read = scanf("%lf ", &t);
    push(t);
    //printf("%lf\n", t);
    while (read != EOF) {
        read = scanf("%lf ", &t);
        if (read != EOF) read = scanf("%c ", &op);
        if (read != EOF) {
            push(t);
            operate(op);
            //printf("%lf\n", t);
            //printf("%c\n", op);
        }
    }
    printf("%lf\n", pop());
    //printf("done\n");
}


